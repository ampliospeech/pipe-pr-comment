# Bitbucket Pipelines Pipe: Comment on pr

This pipe is here to have a simple way of commenting on PR's
using APP password with a username

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: ampliospeech/comment-on-pr:0.1.7
    variables:
      MESSAGE: "<string>"
      USERNAME: "<string>"
      PASSWORD: "<string>"
      BITBUCKET_REPO_OWNER: "<string>"
      BITBUCKET_REPO_SLUG: "<string>"
      BITBUCKET_PR_ID: "<integer>"
      # DEBUG: "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| NAME (*)              | The name that will be printed in the logs |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by bitbucketci-team@atlassian.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
