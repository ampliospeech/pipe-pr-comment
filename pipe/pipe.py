import requests
from bitbucket_pipes_toolkit import Pipe, get_logger

logger = get_logger()

schema = {
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
    'MESSAGE': {'type': 'string', 'required': True},
    'USERNAME': {'type': 'string', 'required': True},
    'PASSWORD': {'type': 'string', 'required': True},
    'BITBUCKET_REPO_OWNER': {'type': 'string', 'required': True},
    'BITBUCKET_REPO_SLUG': {'type': 'string', 'required': True},
    'BITBUCKET_PR_ID': {'type': 'integer', 'required': True},
}
# - curl -H "Content-Type:application/json" -X POST -u "yuriritvin:${BITBUCKET_APP_PASS}" -d "{\"content\":{\"raw\":$RESULT_LINT}}" ""

BASE_URL = "https://api.bitbucket.org/2.0/repositories/{}/{}/pullrequests/{}/comments"


class DemoPipe(Pipe):
    def run(self):
        super().run()
        logger.info('Executing the pipe...')

        try:
            repo_name = self.get_variable('BITBUCKET_REPO_OWNER')
            repo_slug = self.get_variable('BITBUCKET_REPO_SLUG')
            pr_id = self.get_variable('BITBUCKET_PR_ID')
            bitbucket_url_comment = BASE_URL.format(repo_name, repo_slug, pr_id)
            logger.info('Commenting on %s', bitbucket_url_comment)
            msg = {
                "content": {
                    "html": self.get_variable("MESSAGE")
                }
            }
            user = self.get_variable("USERNAME")
            password = self.get_variable("PASSWORD")
            resp = requests.post(bitbucket_url_comment, json=msg, auth=(user, password))
            if 400 <= resp.status_code < 500:
                logger.error(str(resp.content))
            resp.raise_for_status()

            if 300 > resp.status_code >= 200:
                logger.info(str(resp.content))
        except Exception as e:
            self.fail("failed to send comment to bitbucket {}".format(e))
        self.success(message="Success!")


if __name__ == '__main__':
    pipe = DemoPipe(pipe_metadata='/pipe.yml', schema=schema)
    pipe.run()
