# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.7

- patch: change raw comment to html

## 0.1.6

- patch: change to report - test4

## 0.1.5

- patch: change to report - test3

## 0.1.4

- patch: change to report - test2

## 0.1.3

- patch: change to report - test

## 0.1.2

- patch: update image name

## 0.1.1

- patch: Fix security vulnerability.

## 0.1.0

- minor: Initial release

